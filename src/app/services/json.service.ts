import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class JsonService {
  token: any;
  constructor(private http: HttpClient) { }

getJson()
{
  var url = 'https://api.cyrustheapp.com/api/qr';
  return this.http.get(url);
}
postJson(data: any)
{
var url = 'https://api.cyrustheapp.com/api/qr/refered';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postRegister(data: any)
{
var url = 'https://api.cyrustheapp.com/api/register';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postXp(data: any)
{
var url = 'https://api.cyrustheapp.com/api/xp/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postPointshow(data: any)
{
var url = 'https://api.cyrustheapp.com/api/points/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postShowPoints(data: any)
{
var url = 'https://api.cyrustheapp.com/api/points/show';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postPoints(data: any)
{
var url = 'https://api.cyrustheapp.com/api/points';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
postFunction(data: any)
{
var url = 'https://api.cyrustheapp.com/api/function/inventory';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


postReservations(data: any)
{

var url = 'https://api.cyrustheapp.com/api/reservation';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});

}


GetSearch()
{
  var url = 'https://api.cyrustheapp.com/api/search';
  return this.http.get(url);

}


getDataAllPromotions(){
  
  var url = 'http://api.cyrustheapp.com/api/promotionshow';
  return this.http.get(url);
  
  
}
getCategory(){
  
  var url = 'http://api.cyrustheapp.com/api/category/show';
  return this.http.get(url);
  
}

PostSearch(data: any)
{

var url = 'https://api.cyrustheapp.com/api/search';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});

}
ClickOnApromotion(data: any)
  {
  
  var url = 'http://api.cyrustheapp.com/api/promotions';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

PostPromotion(data: any)
  {
  
  var url = 'http://api.cyrustheapp.com/api/promotion';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

PostFavoritelike(data: any)
  {
  
  var url = 'https://api.cyrustheapp.com/api/favorite/like';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

PostFavoritePromotion(data: any)
  {
  
  var url = 'https://api.cyrustheapp.com/api/favorite/promotion/add';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

PostFavoriteLocal(data: any)
  {
  
  var url = 'https://api.cyrustheapp.com/api/favorite/local/add';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  
  }

reviewshow(data: any)
  {
  var url = 'https://api.cyrustheapp.com/api/reviews/show';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
  }
reviewinsert(data: any)
{
var url = 'https://api.cyrustheapp.com/api/rating/promo';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}


RatingLocal(data: any)
{
var url = 'https://api.cyrustheapp.com/api/rating/local';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
 }


GetLocals()
 {
   var url = 'https://api.cyrustheapp.com/api/show/local';
   return this.http.get(url);
 }

ShowLocals(data: any)
 {
  var url = 'https://api.cyrustheapp.com/api/show/local/promo';
  return this.http.post(url,data,
  {headers:new HttpHeaders({"Content-Type":'application/json'})});
 }

ValidateGoogle(data: any)
{
var url = 'https://api.cyrustheapp.com/api/google/validate';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}
UpdateGoogle(data: any)
{
var url = 'https://api.cyrustheapp.com/api/google/update';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

UserGoogle(data: any)
{
var url = 'https://api.cyrustheapp.com/api/google/login';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

GetUserData(data: any)
{
var url = 'https://api.cyrustheapp.com/api/get/user/data';
return this.http.post(url,data,
{headers:new HttpHeaders({"Content-Type":'application/json'})});
}

}
