import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Google Maps Services
// import { GoogleMaps } from '@ionic-native/google-maps';//Google Map
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
//End Google Maps Services

//Native Storage
import { NativeStorage } from '@ionic-native/native-storage/ngx';
//End Native Storage

//Utilities Plugins
import { File } from '@ionic-native/file/ngx';
import { ProgressBarModule } from "angular-progress-bar"
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { IonicRatingModule } from 'ionic4-rating';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
//End Utilities Plugins

//Services
import { JsonService } from './services/json.service';
//End Services


//Login con Email normal
import { LaravelPassportModule } from 'laravel-passport';
//Fin Login con Email normal

//Login Google
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { environment } from '../environments/environment';


//Fin Login Google


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    NgxQRCodeModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicRatingModule,
    ProgressBarModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    LaravelPassportModule.forRoot({apiRoot:'http://api.cyrustheapp.com',
    clientId: 4,
    clientSecret: 'kZ4xUzC0jf3mhZrJ9JgHQwEdJDSM0FGj1CnrlMX2'
        })],

  providers: [
    File,
    SocialSharing,
    JsonService,
    Geolocation,
    NativeStorage,
    NativeGeocoder,
    BarcodeScanner,
    // GoogleMaps,
    GooglePlus,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
