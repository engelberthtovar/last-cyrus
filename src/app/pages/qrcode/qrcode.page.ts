import { Component, OnInit } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
//import { File } from '@ionic-native/file/ngx';



@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.page.html',
  styleUrls: ['./qrcode.page.scss'],
})
export class QrcodePage implements OnInit {
  // data: any;
  // url: any;
  id: any;
  res: any
  name: any;
  last_name: any;
  email: any;
  QrToken :any;
  Token :any;
  tokel : any;
  createdCode: any;



  constructor(   
    private json: JsonService,
    private socialSharing: SocialSharing,
    //private file: File
  ) { 
    localStorage.getItem('currentUser');
    

  }
  ngOnInit()
  {
    var localemail = localStorage['currentUser'];
    var email = {email: localemail};
    // console.log(email);
  
    this.json.postJson(email).subscribe((res: any ) =>{
    var qr_token = res['0']['qr_token'];
    this.QrToken = "http://api.cyrustheapp.com/api/refered"+qr_token;
    // console.log(res);
  });
  }



createCode()
{
    this.json.getJson().subscribe((res: any) =>{
    this.createdCode = this.QrToken;
});
}


 //social networks in sharing
 async shareWhatsApp(){
  // Text + Image or URL works 
    this.socialSharing.shareViaWhatsApp(this.QrToken, null).then(() =>{
      //success
    }).catch((e) => {
      //Error
    })
  }

  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.QrToken, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

}
