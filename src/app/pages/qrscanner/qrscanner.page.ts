import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-qrscanner',
  templateUrl: './qrscanner.page.html',
  styleUrls: ['./qrscanner.page.scss'],
})
export class QrscannerPage implements OnInit {

  scannedCode = null;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private socialSharing: SocialSharing
    ) { }

  ngOnInit() {
  }



  scanCode()
  {
    this.barcodeScanner.scan().then(barcodeData =>{
      this.scannedCode = barcodeData.text;
      
    });
  }

 //social networks in sharing
 async shareWhatsApp(){
  // Text + Image or URL works 
    this.socialSharing.shareViaWhatsApp(this.scannedCode, null).then(() =>{
      //success
    }).catch((e) => {
      //Error
    })
  }

  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.scannedCode, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.scannedCode, null).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

}
