import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrPromotionPage } from './qr-promotion.page';

describe('QrPromotionPage', () => {
  let component: QrPromotionPage;
  let fixture: ComponentFixture<QrPromotionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrPromotionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrPromotionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
