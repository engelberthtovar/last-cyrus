import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { JsonService } from 'src/app/services/json.service';

import { IonicModule } from '@ionic/angular';

import { QrPromotionPage } from './qr-promotion.page';
import { NgxQRCodeModule } from 'ngx-qrcode2';

const routes: Routes = [
  {
    path: '',
    component: QrPromotionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule,
    RouterModule.forChild(routes)
  ],
  providers: [
  
    JsonService
 
  ],
  declarations: [QrPromotionPage]
})
export class QrPromotionPageModule {}
