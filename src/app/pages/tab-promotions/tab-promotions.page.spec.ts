import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabPromotionsPage } from './tab-promotions.page';

describe('TabPromotionsPage', () => {
  let component: TabPromotionsPage;
  let fixture: ComponentFixture<TabPromotionsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabPromotionsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabPromotionsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
