import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { JsonService } from 'src/app/services/json.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { File } from '@ionic-native/file/ngx';
import { Events, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-promo-description',
  templateUrl: './promo-description.page.html',
  styleUrls: ['./promo-description.page.scss'],
})
export class PromoDescriptionPage implements OnInit {
  localdata: any;
  clickedpromotion: any;
  /*shipping message */
  text= 'South American Comfort Food at Its Best!';
  url= 'https://www.elmonofresh.com/';
  name: any;
  last_name: any;
  total_basic_points: any;
  total_gold_points: any;
  id_local: any;
  local: any;
  localPhoto: any;
  rating_starts: any;
  local_id: any;
  id_function: any;
  point_granted: any;

  /*shipping message */
  constructor(

    private file: File,
    private socialSharing: SocialSharing, 
    private alertCtrl: AlertController,


    private activatedRoute: ActivatedRoute,
    private router: Router, 
    private json: JsonService,
  ) { localStorage.getItem('currentUser'); }

  ngOnInit() {

    var localemail = localStorage['currentUser'];
    var email = {email: localemail};
    
    this.json.postJson(email).subscribe((res: any ) =>{
      this.name = res['0']['name'];
      this.last_name = res['0']['last_name'];
    });
    
    
    this.activatedRoute.queryParams.subscribe(res=>{
      this.clickedpromotion = res;//subscribing response
      this.id_local = res.id_local;//subscribing response
      // console.log("Here, I've the objects",this.clickedpromotion);
      // console.log("Local ID",this.id_local);
      
      this.json.ShowLocals(this.id_local).subscribe((res: any ) =>{
        this.localPhoto = res.photo;
        // console.log('Local', res);
        // console.log('photo', this.localPhoto);
        
        });
     
        var id_function = {id_function: 5};
        this.json.postFunction(id_function).subscribe((res: any ) =>{
    
        this.point_granted = res.point_granted
        this.id_function = res.id;
    
          console.log(this.id_function);
        });
    });



      this.json.postShowPoints(email).subscribe((res: any ) =>{
      this.total_basic_points = res.total_basic_points;
      this.total_gold_points = res.total_gold_points;
      });



  }

  logRatingChange(rating){
    console.log("changed rating local in alone view: ",rating);
    // do your stuff
}

Restorant()
{
   this.router.navigate(['/tabs/restaurant-info'],{queryParams: this.clickedpromotion});
}

onGoToNextPage(event, promotion)
{
  this.router.navigate(['/tabs/qr-promotion'],{queryParams: this.clickedpromotion});
}

fuctionPoints()
{
  var localemail = localStorage['currentUser'];
  var email = {email: localemail};
  var data =
  {
    id_function: this.id_function,
    email: email.email,
    point_granted: this.point_granted,
  };
  console.log(data);
  this.json.postPoints(data).subscribe((res: any ) =>{
  });
}


 //social networks in sharing
 async shareWhatsApp(){
  // Text + Image or URL works 
  this.socialSharing.shareViaWhatsApp(this.text, null, this.url).then(() =>{
    //success
  }).catch((e) => {
    //Error
  })
  }

  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.text, null, this.url).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.text, this.url).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }


  async promoRaiting(event)
{
  const alert = await this.alertCtrl.create({
      
    header: 'Rating',
    // message: "Please Enter yours comments",
    // message: "<ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon>",
    message:'Danos Tu Reseña',
    inputs: [
      {
        name: 'title',
        placeholder: '',
        
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Save',
        handler: data => {
        this.rating_starts = event;
        var localemail = localStorage['currentUser'];
        var email = {email: localemail};

        var rating =
        {
          email: email.email,
          id_local: this.id_local,
          rating_text: data.title,
          rating_starts: this.rating_starts,
        };
    this.json.RatingLocal(rating).subscribe((res: any ) =>{
    });
          console.log('Saved clicked');
        }
      }
    ]
  });
  alert.present();

  }




}
