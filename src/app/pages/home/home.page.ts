import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, LoadingController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  
  @ViewChild('slidMore',{ static: true }) slidMore:IonSlides;
  @ViewChild('slidNear', { static: true }) slidNear: IonSlides;
  @ViewChild('slidRecommen', { static: true }) slidRecommen: IonSlides;
  @ViewChild('slides', { static: true }) slider: IonSlides;
  @ViewChild('slideWithNav', { static: true }) slideWithNav: IonSlides;
  
  lupa: any[] = [];
  promotion_name: any;
  promotion_description: any;
  promotion_local_name: any;
  promotion_name1: any;
  promotion_description1: any;
  promotion_local_name1: any;
  promotion_name2: any;
  promotion_description2: any;
  promotion_local_name2: any;
  // data: any;
  user: any;
  userReady: boolean = false;
  searchTerms: any="";
  food: any;
  search: any;
  
  // jsonData : any;
   sliderOne: any;
   segment = 0;
   
  //Configuration for each Slider
  slideOptsOne = {
    // spaceBetween:1,
    // slidesPerView:1.6,
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };

  sliderConfig ={
    spaceBetween:1,
    slidesPerView:1.8,
    initialSlide: 0,
    //slidesPerView: 1,
    autoplay:true
  };



  promotions: any;
  segundoenvio: any;
  localclickeddata: any;
  promo: any[] = [];
  local: any[] = [];
  promotions_id: any;
  element: any[] = [];;
  element_photo: any[] = [];;

  constructor(
    public menu: MenuController,
    // private googlePlus: GooglePlus,
    // private nativeStorage: NativeStorage,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public loading: LoadingController,
    private json: JsonService,
    private router: Router
  )
   {
    localStorage.getItem('currentUser');
    // console.log(localStorage.email);
   
    this.json.getDataAllPromotions().subscribe((res: any) =>{
      this.promotions = res;
      res.forEach(element => {
        this.element = element.id;
        this.element_photo = element.photo;
        console.log(this.element_photo);
      });
      console.log('promociones', this.promotions);
    });

  this.sliderOne =
  {
    isBeginningSlide: true,
    isEndSlide: false,
    slidesItems: [
      {
        id: this.element,
        image: this.element_photo
      },
      // {
      //   id: 2,
      //   // image: '/src/assets/image/1.jpg'
      // },
      // {
      //   id: 3,
      //   // image: '/src/assets/image/1.jpg'
      // }
    ]
  };
}

async ngOnInit()
{
  //agregado el ngOnInit
  //whe need get all promotions without filter by the moments 
  //because we dont erount filter parameters implemented
  this.json.getDataAllPromotions().subscribe((res: any) =>{
    this.promotions = res;
    console.log(this.promotions);
   });
   

}


//evento de click a las mas visitadas:
onGoToNextPage(event, promotion){
 console.log("ID Promotion:",promotion.id);//consolelog of the clicked promotion related local.
       console.log("promotion clicked data before send",promotion);
       this.router.navigate(['/tabs/promo-description'],{queryParams: promotion});
}
ionViewDidLoad() {

 // this.PostSearch();
 // console.log(this.PostSearch);

}

SearchNextPage(event, promotion){
       this.router.navigate(['/tabs/promo-description'],{queryParams: promotion});
}

   //Move to Next slide
   slideNext(object, slideView) {
    slideView.slideNext(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });
  }

  //Move to previous slide
  slidePrev(object, slideView) {
    slideView.slidePrev(500).then(() => {
      this.checkIfNavDisabled(object, slideView);
    });;
  }

  //Method called when slide is changed by drag or navigation
  SlideDidChange(object, slideView) {
    this.checkIfNavDisabled(object, slideView);
  }

  //Call methods to check if slide is first or last to enable disbale navigation  
  checkIfNavDisabled(object, slideView) {
    this.checkisBeginning(object, slideView);
    this.checkisEnd(object, slideView);
  }

  checkisBeginning(object, slideView) {
    slideView.isBeginning().then((istrue) => {
      object.isBeginningSlide = istrue;
    });
  }
  checkisEnd(object, slideView) {
    slideView.isEnd().then((istrue) => {
      object.isEndSlide = istrue;
    });
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }


  PostSearch(event)
  {
    const datas = event.target.value;
    if(datas === '')
    {
      return datas;
    }
    var data = {
      search: datas
    };
    
    // console.log(data);
    this.json.PostSearch(data).subscribe((res: any ) =>{
     this.promo = res;
     this.local = (res['0']['local']);

     console.log(res);
     console.log(this.local);
    //  console.log(res['0']['local']['0']['name']);

    //  this.promotion_name = res['0']['name'];
    //  this.promotion_description = res['0']['description'];
    //  this.promotion_local_name = res['0']['local']['0']['name'];
      
    //  this.searchTerms = [
    //  this.promotion_name,
    //  this.promotion_description,
    //  this.promotion_local_name,
    //   ];
    //  return this.lupa.filter();
    });
  }


}
