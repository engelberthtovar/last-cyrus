import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

//Login Normal
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LaravelPassportService } from 'laravel-passport';

//Login Google
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform } from '@ionic/angular';
import { JsonService } from 'src/app/services/json.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  private user: FormGroup;
  loading: any;

  showPhoneAuth: any = false;
  email: any;
  UserEmail: any;
  constructor(
    public menu: MenuController,
    private laravelPassportService: LaravelPassportService,
    public formBuilder: FormBuilder,
    private splashScreen: SplashScreen,

    private json: JsonService,
    private router: Router,
    private platform: Platform,
    private google:GooglePlus,
    public loadingController: LoadingController,
    private fireAuth: AngularFireAuth
  ) 
  {
    this.user = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });
  }


  async presentLoading() {
    //  const GoogleLoading = 
     const loading = await this.loadingController.create({
        message: 'Connecting ...',
        spinner: 'dots', 
      }).then((overlay) => {
        this.loading = overlay;
        this.loading.present();
      });
        // spinner: 'dots', 
        // duration: 1000
      // return await loading.present();
    }
  
    login() {
  
      const loading = this.loadingController.create({
        message: 'Connecting ...',
        spinner: 'dots', 
      }).then((overlay) => {
        this.loading = overlay;
        this.loading.present();
      });
      const user = this.user.value;
      localStorage.setItem('currentUser', JSON.stringify(user.email));
      this.laravelPassportService.loginWithEmailAndPassword(user.email, user.password).subscribe(
        res => {
          console.log(res);
  
          setTimeout(() => {
            this.loading.dismiss();
            this.router.navigate(['/tabs/home']);
  
          }, 1000);
        },
        err => {
          console.log(err);
        },
        () => {
          console.log('complete');
        }
      );
    }



  
  async LoginGoogle() {
    let params;

    // localStorage.getItem('currentUser')
    // var localemail = localStorage['currentUser'];
    // var email = {email: localemail};

    // if (email != null)
    // {


      if (this.platform.is('android')) {
        params = {
          'webClientId': '305026988335-u4ccfisj2o4tja2v4007og2i45psgrlk.apps.googleusercontent.com',
          'offline': true
        }
      }
      else {
        params = {}
      }
      this.google.login(params)
        .then((response) => {
          const { idToken, accessToken } = response
          this.onLoginSuccess(idToken, accessToken);
        }).catch((error) => {
          console.log(error)
          alert('error:' + JSON.stringify(error))
        });

        // this.router.navigate(["/edit-profile"]);
        // this.loading.dismiss();
    // }else{
    //   this.router.navigate(["/tabs/home"]);
    // }

  }
  onLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
        .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
        .credential(accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        // console.log(response);

        this.email = response["additionalUserInfo"]['profile']['email'];
        // console.log('Console Response', this.email);

        localStorage.setItem('currentUser', JSON.stringify(this.email));

        
        var email = {email: this.email};
        this.json.UserGoogle(email).subscribe((res: any ) =>{
          this.UserEmail = res.email
          // console.log('Respuesta de la Base de Datos', res.email);
          if(this.email == this.UserEmail)
          {
            this.router.navigate(["/tabs/home"]);
            this.loading.dismiss();
          }else{
            
            this.router.navigate(["/edit-profile"]);
            this.loading.dismiss();
          }
          });
      })


  }
  onLoginError(err) {
    console.log(err);
  }


  ionViewWillEnter() {
    
    var res = true;
    if (res === true) {
      this.menu.enable(false);
    }
}

ionViewDidLeave() {
  // enable the root left menu when leaving the tutorial page
  this.menu.enable(true);
}

ionViewDidEnter() {
    this.menu.enable(false, 'start');
    this.menu.enable(false, 'end');
    this.platform.ready().then(() => {
    this.splashScreen.hide();
    this.showPhoneAuth = this.platform.is('desktop') || this.platform.is('pwa')
  });


}



  
}



