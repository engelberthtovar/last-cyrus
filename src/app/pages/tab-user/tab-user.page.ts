import { Component, OnInit } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab-user',
  templateUrl: './tab-user.page.html',
  styleUrls: ['./tab-user.page.scss'],
})
export class TabUserPage implements OnInit {
  insignia: any;
  total_experience: any;
  necessary_experience_for_next_level: any;
  level: any;
  level_name: any;
  user_last_name: any;
  last_name: any;
  user: any;
  name: any;
  total_basic_points: any;
  provider: any;

  constructor(
    private router: Router,
    private json: JsonService
  ) { }

  ngOnInit() {
    var localemail = localStorage['currentUser'];
    var email = {email: localemail};

      this.json.postJson(email).subscribe((res: any ) =>{
      this.name = res['0']['name'];
      this.last_name = res['0']['last_name'];
      this.provider = res['0']['photoURL'];


  });
  this.json.postShowPoints(email).subscribe((res: any ) =>{
  this.total_basic_points = res.total_basic_points;
  });
//      this.json.getJson().subscribe((res: any) =>{
//      this.user = this.name;
//      this.user_last_name = this.last_name;
// });

  this.json.postXp(email).subscribe((exp :any)=>{
  this.insignia = exp['insignia'];
  this.level_name = exp['level_name'];
  this.level = exp['level'];
  this.necessary_experience_for_next_level = exp['necessary_experience_for_next_level'];
  this.total_experience = exp['total_experience'];
  
  

  console.log(exp);
  console.log(exp['level']);

});

  }
  
  
  goToNextPage () {
    this.router.navigate(['/tabs/see-benefits']);
  }

  goToNextLevel(){
    this.router.navigate(['/tabs/see-next-level']);
  }

  goToEarnPoints(){
    this.router.navigate(['/tabs/earn-points']);
  }

}
