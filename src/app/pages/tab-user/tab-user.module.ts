import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ProgressBarModule } from 'angular-progress-bar';

import { IonicModule } from '@ionic/angular';

import { TabUserPage } from './tab-user.page';

const routes: Routes = [
  {
    path: '',
    component: TabUserPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ProgressBarModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabUserPage]
})
export class TabUserPageModule {}
