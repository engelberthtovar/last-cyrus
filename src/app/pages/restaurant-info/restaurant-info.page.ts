import { Component, OnInit, ViewChild  } from '@angular/core';
import {IonSlides} from '@ionic/angular';
import { ActivatedRoute, Router } from "@angular/router";
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AlertController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { JsonService } from 'src/app/services/json.service';


@Component({
  selector: 'app-restaurant-info',
  templateUrl: './restaurant-info.page.html',
  styleUrls: ['./restaurant-info.page.scss'],
})
export class RestaurantInfoPage implements OnInit {
/*shipping message */
text= 'South American Comfort Food at Its Best!';
url= 'https://www.elmonofresh.com/';
/*shipping message */

clicyruspromodata: any;
localdata: any[] = [];
localdescription: any;
localname: any;
rating_starts: any;
local_id: any;
data: any;
localPhoto: any;

constructor(
  private alertCtrl: AlertController,
  private activatedRoute: ActivatedRoute, 
  private router: Router, 
  private socialSharing: SocialSharing, 
  private file: File, 
  private json: JsonService) 


{ 

  this.activatedRoute.queryParams
  .subscribe((res: any)=>{
  this.clicyruspromodata = res;//subscribing response
});


}


ngOnInit() {


  var data = {
    id: this.clicyruspromodata.id
          };
    this.json.ClickOnApromotion(data).subscribe((res: any ) =>{
    // this.localdata = res['0']['local']['0'];
    this.localname = res['0'].local['0'].name;
    this.localdescription = res['0'].local['0'].description;
    this.local_id = res['0'].local['0'].id;
    this.localPhoto = res['0'].local['0'].photo;

    console.log(res);
    //promotion=this.localclickeddata['0']['local'];
  });

}



async localRaiting(event)
{
  const alert = await this.alertCtrl.create({
      
    header: 'Rating',
    // message: "Please Enter yours comments",
    // message: "<ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon><ion-icon name='star'></ion-icon>",
    message:'Danos Tu Reseña',
    inputs: [
      {
        name: 'title',
        placeholder: '',
        
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Save',
        handler: data => {

      this.rating_starts = event;
      var localemail = localStorage['currentUser'];
      var email = {email: localemail};
      var rating =
  {
      email: email.email,
      id_local: this.local_id,
      rating_text: data.title,
      rating_starts: this.rating_starts,
    };
    this.json.RatingLocal(rating).subscribe((res: any ) =>{
      console.log(res);
    });
          console.log('Saved clicked');
        }
      }
    ]
  });

  alert.present();
  // console.log(this.data);
  // this.router.navigate(['/reviewdetails'],{queryParams: raiting});
  }
  
  //social networks in sharing
  async shareWhatsApp(){
   // Text + Image or URL works 
   this.socialSharing.shareViaWhatsApp(this.text, null, this.url).then(() =>{
     //success
   }).catch((e) => {
     //Error
   })
   }
  async shareFacebook(){
    this.socialSharing.shareViaFacebook(this.text, null, this.url).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  shareInstagram(){
    this.socialSharing.shareViaInstagram(this.text, this.url).then((res) => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }

  
  
}