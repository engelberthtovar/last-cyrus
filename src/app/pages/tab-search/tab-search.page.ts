import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JsonService } from 'src/app/services/json.service';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx'


declare var google;

@Component({
  selector: 'app-tab-search',
  templateUrl: './tab-search.page.html',
  styleUrls: ['./tab-search.page.scss'],
})
export class TabSearchPage implements OnInit {
  
  @ViewChild('map',{ static: false }) public mapElement: ElementRef;
  map: any;
  address:string;
  category: Object;

  constructor(
    private geolocation: Geolocation,
    private json: JsonService,
    private nativeGeocoder: NativeGeocoder
  ) { }

  ngOnInit() {
      //Foods Category
  this.json.getCategory().subscribe((res)=>{
    this.category = res;
  console.log(this.category);
  });

 //End Foods Category

    //Loading Google Map
    this.loadMap();
  }

//Google Map
loadMap() {
  this.geolocation.getCurrentPosition().then((resp) => {
    let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
   

    this.map.addListener('tilesloaded', () => {
      console.log('accuracy',this.map);
      this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
    });

  }).catch((error) => {
    console.log('Error getting location', error);
  });
}

getAddressFromCoords(lattitude, longitude) {
  console.log("getAddressFromCoords "+lattitude+" "+longitude);
 
  let options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };

  this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
    .then((result: NativeGeocoderResult[]) => {
      let address = "";
      
      let addMarker = [];
      for (let [key, value] of Object.entries(result[0])) {
        if(value.length>0)
        addMarker.push(value);

      }
      addMarker.reverse();
      for (let value of addMarker) {
        this.address += value+", ";
      }
      address = this.address.slice(0, -2);
    })
    .catch((error: any) =>{ 
      this.address = "Address Not Available!";
    });

}

addMarker(){
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: this.map.getCenter()
    });

    let contenido = "<p>This is your current position !</p>";          
    let infoWindow = new google.maps.InfoWindow({
    content: contenido
    });

    google.maps.event.addListener(marker, 'address', (address) => {
    infoWindow.open(this.map, marker);
    });

}
//End Google Map





//Promo Category
showPromoCategory()
{
this.json.getDataAllPromotions().subscribe((res)=>{
console.log(res);
});
}
//End Promo Category

}
