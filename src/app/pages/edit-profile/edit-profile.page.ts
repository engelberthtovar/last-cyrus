import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { JsonService } from 'src/app/services/json.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

  uid: string;
  displayName: string;
  gmail: string;
  photoURL: string;
  name: any;
  photo: any;
  // user: FormGroup;
  last_name: any;
  phone: any;
  private user :FormGroup;
  userResponse: any;
  
  constructor(
    private router: Router,
    private json: JsonService,
    private formBuilder :FormBuilder,
    public angularFireAuth: AngularFireAuth
  ) {
    this.user = this.formBuilder.group({
      name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone: ['', Validators.required]
    });

  this.angularFireAuth.authState.subscribe(userResponse => { 

       localStorage.setItem('currentUser', JSON.stringify(userResponse.email));
        this.userResponse = userResponse;
        this.uid = userResponse.uid;
        this.displayName = userResponse.displayName;
        this.gmail = userResponse.email;
        this.photoURL = userResponse.photoURL;
   
        var email = userResponse.email;
        this.json.ValidateGoogle(email).subscribe((dataResponse :any ) => {
        console.log('Datos del local store',dataResponse);
        this.name = dataResponse.name;
        console.log(this.name);
        this.photo = dataResponse.photoURL;

      });
  })

   }
  ngOnInit() {
    localStorage.getItem('currentUser');
    
  }


  async saveData() {
    console.log('Datos recibidos por el Formulario', this.user.value);
    this.name = this.user.value.name;
    this.last_name = this.user.value.last_name
    this.phone = this.user.value.phone;

    var data = {
      email: this.gmail,
      name: this.name,
      last_name: this.last_name,
      phone: this.phone,
      photoURL: this.photoURL,
      provider_id: this.uid,
      provider: 'Google',
    };
    
    this.json.UpdateGoogle(data).subscribe((res: any ) =>{

    console.log(res);
    this.router.navigate(['/tabs/tab-user']);
  });
  

  }

}
