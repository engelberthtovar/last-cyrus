import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MorePointsPage } from './more-points.page';

describe('MorePointsPage', () => {
  let component: MorePointsPage;
  let fixture: ComponentFixture<MorePointsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MorePointsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MorePointsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
