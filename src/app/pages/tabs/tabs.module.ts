import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
      { path: 'promo-description', loadChildren: '../promo-description/promo-description.module#PromoDescriptionPageModule' },
      { path: 'restaurant-info', loadChildren: '../restaurant-info/restaurant-info.module#RestaurantInfoPageModule' },
      { path: 'tab-user', loadChildren: '../tab-user/tab-user.module#TabUserPageModule' },
      { path: 'points', loadChildren: '../points/points.module#PointsPageModule' },
      { path: 'more-points', loadChildren: '../more-points/more-points.module#MorePointsPageModule' },
      { path: 'tab-search', loadChildren: '../tab-search/tab-search.module#TabSearchPageModule' },
      { path: 'tab-promotions', loadChildren: '../tab-promotions/tab-promotions.module#TabPromotionsPageModule' },
      { path: 'qr-promotion', loadChildren: '../qr-promotion/qr-promotion.module#QrPromotionPageModule' },
      { path: 'qrcode', loadChildren: '../qrcode/qrcode.module#QrcodePageModule' },
      { path: 'favorites', loadChildren: '../favorites/favorites.module#FavoritesPageModule' },
      { path: 'qrscanner', loadChildren: '../qrscanner/qrscanner.module#QrscannerPageModule' },
     

      // { path: 'see-benefits', loadChildren: '../see-benefits/see-benefits.module#SeeBenefitsPageModule' },
      // { path: 'see-next-level', loadChildren: '../see-next-level/see-next-level.module#SeeNextLevelPageModule' },
      // { path: 'earn-points', loadChildren: '../earn-points/earn-points.module#EarnPointsPageModule' },
      // { path: 'edit-profile', loadChildren: '../edit-profile/edit-profile.module#EditProfilePageModule' },
      // { path: 'gallery', loadChildren: '../gallery/gallery.module#GalleryPageModule' },
    ]
    // { path: 'tab-star', loadChildren: '../tab-star/tab-star.module#TabStarPageModule' },//Ahora es tab-promotions
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule, 
    IonicModule,
    RouterModule.forChild(routes)
  ],

  providers: [
    // GoogleMaps
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}