import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)},
  // { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  // { path: 'promo-description', loadChildren: './pages/promo-description/promo-description.module#PromoDescriptionPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'qrcode', loadChildren: './pages/qrcode/qrcode.module#QrcodePageModule' },
  { path: 'favorites', loadChildren: './pages/favorites/favorites.module#FavoritesPageModule' },
  { path: 'qr-promotion', loadChildren: './pages/qr-promotion/qr-promotion.module#QrPromotionPageModule' },
  { path: 'qrscanner', loadChildren: './pages/qrscanner/qrscanner.module#QrscannerPageModule' },

  // { path: 'tab-promotions', loadChildren: './pages/tab-promotions/tab-promotions.module#TabPromotionsPageModule' },
  // { path: 'tab-search', loadChildren: './pages/tab-search/tab-search.module#TabSearchPageModule' },
  // { path: 'more-points', loadChildren: './pages/more-points/more-points.module#MorePointsPageModule' },
  // { path: 'restaurant-info', loadChildren: './pages/restaurant-info/restaurant-info.module#RestaurantInfoPageModule' },
  // { path: 'tab-user', loadChildren: './pages/tab-user/tab-user.module#TabUserPageModule' },
 
  
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
