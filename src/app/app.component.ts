import { Component } from '@angular/core';

import { Platform, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { JsonService } from './services/json.service';
import { TabUserPage } from './pages/tab-user/tab-user.page';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rootPage:any = './login/login.page';
  navigate : any;
  user: any;
  private isUserLoggedIn;
  public usserLogged:TabUserPage;
  
  nav: NavController;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    public router: Router,
    public storage: NativeStorage,
    public menu: MenuController,
    private fireAuth: AngularFireAuth,
    public json: JsonService,
    private statusBar: StatusBar
  ) 
  {
    this.LocalStorageSessionGet();
    this.initializeApp();
    this.sideMenu();
  }
  LocalStorageSessionGet()
  {
    console.log(localStorage);
    return JSON.parse(localStorage.getItem('access_token'));
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    // Goole inicio
    this.platform.ready().then(() => {
      this.fireAuth.auth.onAuthStateChanged(user => {
        if (user) {
          this.router.navigate(["/tabs/home"]);
          this.splashScreen.hide();
        }
        else {
          this.router.navigate(["/login"]);
          this.splashScreen.hide();
        }
      })
      this.statusBar.styleDefault();
    });
  



  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "Profile",
        url   : "/tabs/tab-user",
        icon  : "person"
      },
      {
        title : "Search",
        url   : "/tabs/tab-search",
        icon  : "search"
      },
      {
        title : "QR Generate",
        url   : "/tabs/qrcode",
        icon  : "barcode"  
      },
      // {
      //   title : "Inbox",
      //   url   : "#",
      //   icon  : "mail-open"
      // },
      {
        title : "Favorites",
        url   : "/tabs/favorites",
        icon  : "heart-empty"
      },
      {
        title : "My points",
        url   : "/tabs/points",
        icon  : "happy"
      },
      {
        title : "Qr-Scanner",
        url   : "/tabs/qrscanner",
        icon  : "qr-scanner"
      },
      
    ]
  }

  ionViewWillEnter() {
    this.storage.getItem('first').then(res => {
      if (res === true) {
        this.router.navigateByUrl('/tabs/home');
      }
    });

    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  logoutClicked(){
    // this.menu.enable(false);
    localStorage.removeItem('currentUser');
    localStorage.clear();
    //this.backToWelcome();
    console.log(localStorage);
   // this.menu.close();
    this.router.navigate(['/login']);
  }
}
