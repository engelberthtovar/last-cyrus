// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
  apiKey: "AIzaSyDt2KiHM-KoNlCduElYYgTVe-fQoKy0rz0",
  authDomain: "cyrus-movil.firebaseapp.com",
  databaseURL: "https://cyrus-movil.firebaseio.com",
  projectId: "cyrus-movil",
  storageBucket: "cyrus-movil.appspot.com",
  messagingSenderId: "305026988335",
  appId: "1:305026988335:web:4f07acf85e52a09d"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
